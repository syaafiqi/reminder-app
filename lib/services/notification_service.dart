import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_timezone/flutter_timezone.dart';
import 'package:reminder_app/models/reminder.dart';
import 'package:rxdart/subjects.dart';
import 'package:timezone/timezone.dart' as tz;

class NotificationService {
  static final FlutterLocalNotificationsPlugin
      _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  final BehaviorSubject<String> behaviorSubject = BehaviorSubject();

  Future<void> init() async {
    final timeZoneName = await FlutterTimezone.getLocalTimezone();
    tz.setLocalLocation(tz.getLocation(timeZoneName));

    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    final DarwinInitializationSettings initializationSettingsIOS =
        DarwinInitializationSettings(
      onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );
    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );

    await _flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: onDidReceiveLocalNotificationResponse,
    );

    if (Platform.isIOS) {
      await _flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
            alert: true,
            badge: true,
            sound: true,
          );
    }
    if (Platform.isAndroid) {
      _flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.requestNotificationsPermission();
    }
  }

  void onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) {
    // handle notification received logic here
    if (kDebugMode) {
      print("notification received: $id");
    }

    behaviorSubject.add(id.toString());
  }

  void onDidReceiveLocalNotificationResponse(NotificationResponse response) {
    // handle notification tapped logic here
    if (kDebugMode) {
      print("notification tapped: ${response.id}");
    }

    behaviorSubject.add(response.id.toString());
  }

  Future<void> scheduleNotification(Reminder reminder) async {
    _flutterLocalNotificationsPlugin.zonedSchedule(
      reminder.id.hashCode,
      reminder.title,
      'Time to ${reminder.title}',
      tz.TZDateTime.from(reminder.time, tz.local),
      notificationDetails(),
      androidScheduleMode: AndroidScheduleMode.exactAllowWhileIdle,
      uiLocalNotificationDateInterpretation:
          UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

  static NotificationDetails notificationDetails() {
    return const NotificationDetails(
      android: AndroidNotificationDetails(
        'reminder_channel',
        'Reminder Channel',
        channelDescription: 'Channel for Reminder notifications',
        importance: Importance.max,
        priority: Priority.high,
        ticker: "ticker",
        playSound: true,
        enableVibration: true,
      ),
    );
  }

  Future<void> cancelNotification(String reminderId) async {
    await _flutterLocalNotificationsPlugin.cancel(reminderId.hashCode);
  }
}
