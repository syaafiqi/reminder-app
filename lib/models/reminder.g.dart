// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reminder.dart';

// **************************************************************************
// RealmObjectGenerator
// **************************************************************************

class Reminder extends _Reminder
    with RealmEntity, RealmObjectBase, RealmObject {
  Reminder(
    String id,
    String title,
    DateTime time,
    double latitude,
    double longitude,
    String placeName,
  ) {
    RealmObjectBase.set(this, 'id', id);
    RealmObjectBase.set(this, 'title', title);
    RealmObjectBase.set(this, 'time', time);
    RealmObjectBase.set(this, 'latitude', latitude);
    RealmObjectBase.set(this, 'longitude', longitude);
    RealmObjectBase.set(this, 'placeName', placeName);
  }

  Reminder._();

  @override
  String get id => RealmObjectBase.get<String>(this, 'id') as String;
  @override
  set id(String value) => RealmObjectBase.set(this, 'id', value);

  @override
  String get title => RealmObjectBase.get<String>(this, 'title') as String;
  @override
  set title(String value) => RealmObjectBase.set(this, 'title', value);

  @override
  DateTime get time => RealmObjectBase.get<DateTime>(this, 'time') as DateTime;
  @override
  set time(DateTime value) => RealmObjectBase.set(this, 'time', value);

  @override
  double get latitude =>
      RealmObjectBase.get<double>(this, 'latitude') as double;
  @override
  set latitude(double value) => RealmObjectBase.set(this, 'latitude', value);

  @override
  double get longitude =>
      RealmObjectBase.get<double>(this, 'longitude') as double;
  @override
  set longitude(double value) => RealmObjectBase.set(this, 'longitude', value);

  @override
  String get placeName =>
      RealmObjectBase.get<String>(this, 'placeName') as String;
  @override
  set placeName(String value) => RealmObjectBase.set(this, 'placeName', value);

  @override
  Stream<RealmObjectChanges<Reminder>> get changes =>
      RealmObjectBase.getChanges<Reminder>(this);

  @override
  Reminder freeze() => RealmObjectBase.freezeObject<Reminder>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObjectBase.registerFactory(Reminder._);
    return const SchemaObject(ObjectType.realmObject, Reminder, 'Reminder', [
      SchemaProperty('id', RealmPropertyType.string, primaryKey: true),
      SchemaProperty('title', RealmPropertyType.string),
      SchemaProperty('time', RealmPropertyType.timestamp),
      SchemaProperty('latitude', RealmPropertyType.double),
      SchemaProperty('longitude', RealmPropertyType.double),
      SchemaProperty('placeName', RealmPropertyType.string),
    ]);
  }
}
