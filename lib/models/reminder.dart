import 'package:realm/realm.dart';

part 'reminder.g.dart';

@RealmModel()
class _Reminder {
  @PrimaryKey()
  late String id;
  late String title;
  late DateTime time;
  late double latitude;
  late double longitude;
  late String placeName;
}
