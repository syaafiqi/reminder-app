class LocationModel {
  String name;
  String simpleName;
  double latitude;
  double longitude;

  LocationModel({
    required this.name,
    required this.simpleName,
    required this.latitude,
    required this.longitude,
  });

  factory LocationModel.fromMap(Map<String, dynamic> json) => LocationModel(
        name: json['name'],
        simpleName: json['simple_name'],
        latitude: json['latitude'],
        longitude: json['longitude'],
      );
}
