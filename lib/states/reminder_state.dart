import 'package:equatable/equatable.dart';
import 'package:reminder_app/models/reminder.dart';

abstract class ReminderState extends Equatable {
  const ReminderState();

  @override
  List<Object> get props => [];
}

class ReminderLoading extends ReminderState {}

class ReminderLoaded extends ReminderState {
  final List<Reminder> reminders;

  const ReminderLoaded([this.reminders = const []]);

  @override
  List<Object> get props => [reminders];
}

class ReminderError extends ReminderState {}

class ReminderFormValid extends ReminderState {
  final bool isValid;
  const ReminderFormValid(this.isValid);
}
