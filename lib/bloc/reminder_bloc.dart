import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:reminder_app/events/reminder_event.dart';
import 'package:reminder_app/models/location.dart';
import 'package:reminder_app/services/notification_service.dart';
import 'package:reminder_app/states/reminder_state.dart';
import 'package:reminder_app/models/reminder.dart';
import 'package:realm/realm.dart';

class ReminderBloc extends Bloc<ReminderEvent, ReminderState> {
  final Realm realm;
  final NotificationService notificationService;

  ReminderBloc(this.realm, this.notificationService)
      : super(ReminderLoading()) {
    on<LoadReminders>(_onLoadReminders);
    on<AddReminder>(_onAddReminder);
    on<DeleteReminder>(_onDeleteReminder);
    on<ValidateReminderForm>(_onValidateReminderForm);
  }

  void _onLoadReminders(LoadReminders event, Emitter<ReminderState> emit) {
    final reminders =
        realm.query<Reminder>('TRUEPREDICATE SORT(time DESC)').toList();
    emit(ReminderLoaded(reminders));
  }

  void _onAddReminder(AddReminder event, Emitter<ReminderState> emit) {
    realm.write(() {
      realm.add(event.reminder);
    });
    notificationService.scheduleNotification(event.reminder);

    add(LoadReminders());
  }

  void _onDeleteReminder(DeleteReminder event, Emitter<ReminderState> emit) {
    final reminder = realm.find<Reminder>(event.id);
    String remiderId = "";
    if (reminder != null) {
      remiderId = reminder.id;
      realm.write(() {
        realm.delete(reminder);
      });
      notificationService.cancelNotification(remiderId);
    }
    add(LoadReminders());
  }

  void _onValidateReminderForm(
      ValidateReminderForm event, Emitter<ReminderState> emit) {
    bool isValid =
        _validateForm(event.title, event.date, event.time, event.place);
    emit(ReminderFormValid(isValid));
  }

  bool _validateForm(
      String title, DateTime date, TimeOfDay time, LocationModel? place) {
    return title.isNotEmpty &&
        date
            .copyWith(hour: time.hour, minute: time.minute)
            .isAfter(DateTime.now()) &&
        place != null;
  }
}
