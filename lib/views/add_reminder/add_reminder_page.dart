import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:reminder_app/bloc/reminder_bloc.dart';
import 'package:reminder_app/events/reminder_event.dart';
import 'package:reminder_app/models/location.dart';
import 'package:reminder_app/models/reminder.dart';
import 'package:reminder_app/states/reminder_state.dart';
import 'package:reminder_app/utils/extensions.dart';

class AddReminderPage extends StatefulWidget {
  @override
  _AddReminderPageState createState() => _AddReminderPageState();
}

class _AddReminderPageState extends State<AddReminderPage> {
  final _formKey = GlobalKey<FormState>();
  final _titleController = TextEditingController();
  final _dateController = TextEditingController();
  final _timeController = TextEditingController();
  final _placeController = TextEditingController();

  LocationModel? _selectedLocation;
  DateTime _selectedDate = DateTime.now();
  TimeOfDay _selectedTime = TimeOfDay.now();

  final DateFormat _viewDateFormat = DateFormat('dd MMMM yyyy');
  final DateFormat _systemDateFormat = DateFormat('yyyy-MM-dd');

  Future<List<LocationModel>> _getSuggestions(String query) async {
    if (query.isEmpty) {
      return [];
    }
    const String baseUrl = 'https://nominatim.openstreetmap.org/search';
    final String formattedQuery = query.replaceAll(' ', '+');
    final String url =
        '$baseUrl?q=$formattedQuery&format=json&addressdetails=1&limit=10';

    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final List data = json.decode(response.body);
      return data.map((place) {
        return LocationModel(
          name: place['display_name'],
          simpleName: place['name'],
          latitude: double.parse(place['lat']),
          longitude: double.parse(place['lon']),
        );
      }).toList();
    } else {
      throw Exception('Failed to load suggestions');
    }
  }

  Future<void> _onPlaceSelected(LocationModel suggestion) async {
    setState(() {
      _selectedLocation = suggestion;
      _placeController.text = suggestion.name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Reminder'),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                _buildTextField("Title", _titleController, isRequired: true),
                _buildTextField(
                  "Date",
                  _dateController,
                  isRequired: true,
                  onTap: () async {
                    final DateTime? picked = await showDatePicker(
                      context: context,
                      initialDate: _selectedDate,
                      firstDate: DateTime.now(),
                      lastDate:
                          DateTime.parse("${DateTime.now().year + 20}-12-31"),
                    );
                    if (picked != null && picked != _selectedDate) {
                      setState(() {
                        _selectedDate = picked;
                        _dateController.text =
                            _viewDateFormat.format(_selectedDate);
                      });
                    }
                  },
                ),
                _buildTextField(
                  "Time",
                  _timeController,
                  isRequired: true,
                  onTap: () async {
                    final TimeOfDay? picked = await showTimePicker(
                      context: context,
                      initialTime: _selectedTime,
                      builder: (context, child) => MediaQuery(
                        data: MediaQuery.of(context)
                            .copyWith(alwaysUse24HourFormat: true),
                        child: child!,
                      ),
                    );
                    if (picked != null && picked != _selectedTime) {
                      setState(() {
                        _selectedTime = picked;
                        _timeController.text = picked.to24hours();
                      });
                    }
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TypeAheadFormField<LocationModel>(
                    textFieldConfiguration: TextFieldConfiguration(
                      controller: _placeController,
                      decoration: InputDecoration(
                        labelText: 'Place',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value == "") {
                        return "Place is required";
                      }
                      return null;
                    },
                    suggestionsCallback: _getSuggestions,
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        title: Text(suggestion.name),
                      );
                    },
                    onSuggestionSelected: (suggestion) {
                      _onPlaceSelected(suggestion);
                    },
                  ),
                ),
                BlocBuilder<ReminderBloc, ReminderState>(
                  builder: (context, state) {
                    if (state is ReminderFormValid) {
                      return state.isValid
                          ? Text('Form is valid',
                              style: TextStyle(color: Colors.green))
                          : Text('Form is invalid',
                              style: TextStyle(color: Colors.red));
                    }
                    return Container();
                  },
                ),
                SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      final reminder = Reminder(
                        DateFormat("yyyyMMddHHmmss").format(DateTime.now()),
                        _titleController.text,
                        DateTime.parse(
                            "${_systemDateFormat.format(_selectedDate)} ${_timeController.text}:00"),
                        _selectedLocation?.latitude ?? 0.0,
                        _selectedLocation?.longitude ?? 0.0,
                        _selectedLocation?.simpleName ?? "",
                      );

                      BlocProvider.of<ReminderBloc>(context)
                          .add(AddReminder(reminder));
                      Navigator.pop(context);
                    } else {
                      BlocProvider.of<ReminderBloc>(context).add(
                        ValidateReminderForm(
                          _titleController.text,
                          _selectedDate,
                          _selectedTime,
                          _selectedLocation,
                        ),
                      );
                    }
                  },
                  child: Text('Add Reminder'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTextField(String labelText, TextEditingController controller,
      {void Function()? onTap, bool isRequired = false}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        onTap: onTap,
        controller: controller,
        decoration: InputDecoration(
          labelText: labelText,
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (isRequired && (value == null || value.isEmpty)) {
            return '$labelText is required';
          }
          return null;
        },
      ),
    );
  }
}
