import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:reminder_app/bloc/reminder_bloc.dart';
import 'package:reminder_app/events/reminder_event.dart';
import 'package:reminder_app/services/notification_service.dart';
import 'package:reminder_app/states/reminder_state.dart';
import 'package:reminder_app/views/add_reminder/add_reminder_page.dart';
import 'package:timezone/timezone.dart' as tz;

class ReminderPage extends StatelessWidget {
  final NotificationService notificationService;

  const ReminderPage({required this.notificationService});

  void listenToNotificationStream(BuildContext context) =>
      notificationService.behaviorSubject.listen((payload) {
        BlocProvider.of<ReminderBloc>(context).add(LoadReminders());
      });

  @override
  Widget build(BuildContext context) {
    // Can't find notification received listener for Android. Notification have to be clicked to refresh page.
    if (Platform.isIOS) {
      listenToNotificationStream(context);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Reminders'),
      ),
      body: BlocBuilder<ReminderBloc, ReminderState>(
        builder: (context, state) {
          if (state is ReminderLoading) {
            return Center(child: CircularProgressIndicator());
          } else if (state is ReminderLoaded) {
            return ListView.builder(
              itemCount: state.reminders.length,
              itemBuilder: (context, index) {
                final reminder = state.reminders[index];
                final date = DateFormat('dd MMMM yyyy').format(reminder.time);
                final time =
                    DateFormat('HH:mm').format(reminder.time.toLocal());
                return Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ListTile(
                    title: Text(reminder.title),
                    subtitle: Padding(
                      padding: const EdgeInsets.only(top: 6),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Date: $date | Time: $time"),
                            Text("Location: ${reminder.placeName}"),
                            tz.TZDateTime.from(reminder.time, tz.local)
                                        .compareTo(tz.TZDateTime.from(
                                            DateTime.now(), tz.local)) <
                                    0
                                ? Text(
                                    "Event Has Passed.",
                                    style: TextStyle(color: Colors.red),
                                  )
                                : Container(),
                          ]),
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        BlocProvider.of<ReminderBloc>(context)
                            .add(DeleteReminder(reminder.id));
                      },
                    ),
                  ),
                );
              },
            );
          } else {
            return Center(child: Text('Failed to load reminders'));
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => BlocProvider.value(
                      value: BlocProvider.of<ReminderBloc>(context),
                      child: AddReminderPage(),
                    )),
          );
        },
      ),
    );
  }
}
