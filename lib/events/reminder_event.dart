import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:reminder_app/models/location.dart';
import 'package:reminder_app/models/reminder.dart';

abstract class ReminderEvent extends Equatable {
  const ReminderEvent();
}

class AddReminder extends ReminderEvent {
  final Reminder reminder;

  const AddReminder(this.reminder);

  @override
  List<Object> get props => [reminder];
}

class DeleteReminder extends ReminderEvent {
  final String id;

  const DeleteReminder(this.id);

  @override
  List<Object> get props => [id];
}

class LoadReminders extends ReminderEvent {
  @override
  List<Object> get props => [];
}

class ValidateReminderForm extends ReminderEvent {
  final String title;
  final DateTime date;
  final TimeOfDay time;
  final LocationModel? place;
  const ValidateReminderForm(this.title, this.date, this.time, this.place);

  @override
  List<Object> get props => [title, date, time, 'place'];
}
