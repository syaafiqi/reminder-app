import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reminder_app/bloc/reminder_bloc.dart';
import 'package:reminder_app/events/reminder_event.dart';
import 'package:reminder_app/services/notification_service.dart';
import 'package:reminder_app/models/reminder.dart';
import 'package:realm/realm.dart';
import 'package:reminder_app/views/reminder/reminder_page.dart';
import 'package:timezone/data/latest.dart' as tz;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  tz.initializeTimeZones();

  final config = Configuration.local([Reminder.schema]);
  final realm = Realm(config);

  final notificationService = NotificationService();
  notificationService.init();

  runApp(MyApp(realm: realm, notificationService: notificationService));
}

class MyApp extends StatelessWidget {
  final Realm realm;
  final NotificationService notificationService;

  const MyApp(
      {Key? key, required this.realm, required this.notificationService})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reminder App',
      home: BlocProvider(
        create: (context) =>
            ReminderBloc(realm, notificationService)..add(LoadReminders()),
        child: ReminderPage(notificationService: notificationService),
      ),
    );
  }
}
