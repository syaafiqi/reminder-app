import 'package:flutter/material.dart';

extension TimeOfDayConverter on TimeOfDay {
  String to24hours() {
    final hourStr = hour.toString().padLeft(2, "0");
    final minStr = minute.toString().padLeft(2, "0");
    return "$hourStr:$minStr";
  }
}
